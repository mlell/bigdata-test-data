BigData Upload Test Cases
=========================

This repository contains data files that represent possible use cases of the web
upload.

The code in this repository serves to create test cases. For phenotypic data,
the original data file from which other test cases are generated is 
`pheno/optional/pheno.xlsx`.

R environment
=============

The used packages are documented via renv. Start R and source `.Rprofile`,
(done automatically if you start R inside this directory, for example when you 
create and open an RStudio project in this folder), then execute
`renv::restore()` to install the needed packages.
